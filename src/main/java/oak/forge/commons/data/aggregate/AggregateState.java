package oak.forge.commons.data.aggregate;

import lombok.*;
import oak.forge.commons.data.version.Version;

import java.io.Serializable;
import java.util.UUID;

@Getter
@Setter
public abstract class AggregateState implements Serializable {

	private static final long serialVersionUID = 1L;

	private final UUID aggregateId;
	private Version version;

	public AggregateState(UUID aggregateId) {
		this(aggregateId, Version.initial());
	}

	protected AggregateState(UUID aggregateId, Version version) {
		this.aggregateId = aggregateId;
		this.version = version;
	}

	@Override
	public String toString() {
		return String.format("[aggregateId: %s; version: %s]", aggregateId, version);
	}
}
