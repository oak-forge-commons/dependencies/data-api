package oak.forge.commons.data.message;

import lombok.*;

import java.time.Instant;
import java.util.UUID;

import static java.lang.String.format;
import static java.time.Instant.now;

@Getter
@Setter
public abstract class Message {

    public static final UUID PUBLIC_RECEIVER_ID = null;

    protected final Instant timestamp;
    protected final UUID receiverId;
    protected UUID senderId;

    // TODO: try to find out if these members can be final
    public String authToken;
    protected UUID correlationId;

    protected Message() {
        this.timestamp = now();
        this.receiverId = PUBLIC_RECEIVER_ID;
    }

    protected Message(UUID receiverId) {
        this.timestamp = now();
        this.receiverId = receiverId;
    }


    @Override
    public String toString() {
        return format("%s: %-22s", timestamp, getClass().getSimpleName());
    }
}
