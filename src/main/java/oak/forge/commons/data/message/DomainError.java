package oak.forge.commons.data.message;

public abstract class DomainError extends Event {

    public DomainError() {
        super();
    }

    @Override
    public String toString() {
        return super.toString() + " (err)";
    }
}
