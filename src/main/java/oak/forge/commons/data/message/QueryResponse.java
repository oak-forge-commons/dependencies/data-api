package oak.forge.commons.data.message;

public abstract class QueryResponse extends Message {

//    protected QueryResponse(Instant timestamp, UUID receiverId) {
//        super(timestamp, receiverId);
//    }

    @Override
    public String toString() {
        return super.toString() + " (qryresp)";
    }
}
