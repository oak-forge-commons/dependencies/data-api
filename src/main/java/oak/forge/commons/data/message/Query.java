package oak.forge.commons.data.message;

import java.util.UUID;

public abstract class Query extends Message {

    protected Query() {
        super();
    }

    protected Query(UUID receiverId) {
        super(receiverId);
    }

    @Override
    public String toString() {
        return super.toString() + " (qry)";
    }
}
