package oak.forge.commons.data.trait;

import java.util.Map;

public interface Values {

    Map<String, String> values();

}
