package oak.forge.commons.data.readmodel;

import oak.forge.commons.data.version.Version;

import java.io.Serializable;
import java.time.Instant;
import java.util.UUID;

import static java.lang.String.format;
import static java.time.Instant.now;
import static java.time.format.DateTimeFormatter.ISO_INSTANT;

public abstract class ReadModel implements Serializable {

	private static final long serialVersionUID = 2655589793494850987L;

	private UUID aggregateId;
	private Version version;
	private Instant creationTimestamp;

	public ReadModel() {
        this.creationTimestamp = now();
	}

	public ReadModel(UUID aggregateId) {
		this.aggregateId = aggregateId;
		this.creationTimestamp = now();
	}

	public UUID getAggregateId() {
		return aggregateId;
	}

	public void setAggregateId(UUID aggregateId) {
		this.aggregateId = aggregateId;
	}

	public Version getVersion() {
		return version;
	}

	public void setVersion(Version version) {
		this.version = version;
	}

	public Instant getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(Instant creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	@Override
	public String toString() {
		String details = toDetailedString();
		return format("%s (aggregateId: %s, version: %s, created: %s)%s",
				getClass().getName(),
				aggregateId,
				version,
				ISO_INSTANT.format(creationTimestamp),
				details == null || details.isEmpty() ? "" : ": " + details
		);
	}

	protected String toDetailedString() {
		return null;
	}
}
