package oak.forge.commons.data.readmodel;

public class InvalidReadModelException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public InvalidReadModelException(String message) {
		super(message);
	}
}
