package oak.forge.commons.data.readmodel;

import oak.forge.commons.data.query.filter.Filter;
import oak.forge.commons.data.version.Version;


import java.util.List;

public interface ReadModelStorage {

	<R extends ReadModel> void store(String readModelName, R readModel, Version version);

	<R extends ReadModel> List<R> get(String readModelName, Filter filter);

	<R extends ReadModel> List<R> getAll(String readModelName);

	void delete(String readModelName, Filter filter);
}
