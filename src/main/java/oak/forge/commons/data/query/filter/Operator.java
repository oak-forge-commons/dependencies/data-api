package oak.forge.commons.data.query.filter;

public enum Operator {

	AND,
	OR,
	NOT,

	EQ,

	NOOP
}
