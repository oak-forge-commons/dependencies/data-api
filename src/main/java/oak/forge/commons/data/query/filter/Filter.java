package oak.forge.commons.data.query.filter;

public interface Filter {

	Operator getOperator();
}
